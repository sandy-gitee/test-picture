source ./set_config.sh
echo "Enter cluster tile test case, Plane id: $1, crtc id: $2, connector id: $3"

echo "----------Test FMT YUV444 TILE 4x4m0 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV24@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV24@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV24@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV24@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_tile4x4_mode0 -v -O -n 2 -t

echo "----------Test FMT YUV422 TILE 4x4m0 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV16@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV16@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV16@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV16@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_tile4x4_mode0 -v -O -n 2 -t

echo "----------Test FMT YUV420 TILE 4x4m0 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV12@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV12@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV12@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV12@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_tile4x4_mode0 -v -O -n 2 -t

echo "----------Test FMT YUV444 TILE 4x4m0 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV30@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV30@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV30@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV30@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv444_10b_tile4x4_mode0 -v -O -n 2 -t

echo "----------Test FMT YUV422 TILE 4x4m0 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV20@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV20@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV20@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV20@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv422_10b_tile4x4_mode0  -v -O -n 2 -t

echo "----------Test FMT YUV420 TILE 4x4m0 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_10b_yuv420_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_10b_tile4x4_mode0 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV15@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_10b_yuv420_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV15@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_10b_tile4x4_mode0 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV15@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_10b_yuv420_tile4x4_mode0 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV15@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_10b_tile4x4_mode0 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m0 -F  ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_10b_yuv420_tile4x4_mode0 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m0 -F ../YUV_TILE/YUV_TILE_4X4_MODE0/320x240_yuv420_10b_tile4x4_mode0  -v -O -n 2 -t

echo "----------Test FMT YUV444 TILE 4x4m1 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV24@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV24@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV24@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV24@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_tile4x4_mode1   -v -O -n 2 -t

echo "----------Test FMT YUV422 TILE 4x4m1 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV16@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV16@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV16@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV16@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_tile4x4_mode1  -v -O -n 2 -t

echo "----------Test FMT YUV420 TILE 4x4m1 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV12@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@NV12@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV12@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@NV12@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_tile4x4_mode1 -v -O -n 2 -t
#echo "test Tile4x4_YCbCr400: io -4 0xf9001000 0x0002111f;io -4 0xf9000000 0xffffffff"
#echo "io -4 0xf9001000 0x0002111f;io -4 0xf9000000 0xffffffff"
#sleep 3;

echo "----------Test FMT YUV444 TILE 4x4m1 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV30@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV30@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV30@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV30@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv444_10b_tile4x4_mode1 -v -O -n 2 -t

echo "----------Test FMT YUV422 TILE 4x4m1 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV20@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV20@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV20@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV20@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv422_10b_tile4x4_mode1 -v -O -n 2 -t

echo "----------Test FMT YUV420 TILE 4x4m1 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV15@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:800x1280+0+0@$NV15@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV15@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:120x140+0+0@$NV15@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m1 -F  ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile4x4m1 -F ../YUV_TILE/YUV_TILE_4X4_MODE1/320x240_yuv420_10b_tile4x4_mode1 -v -O -n 2 -t

#echo "test Tile4x4_YCbCr400_101010: io -4 0xf9001000 0x0002113f;io -4 0xf9000000 0xffffffff"
#echo "io -4 0xf9001000 0x0002113f;io -4 0xf9000000 0xffffffff"
#sleep 3;

# rk3576 can't support tile8x8
exit 0

echo "----------Test FMT YUV420 TILE 8X8 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -t

#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -t

#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV12@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_tile8x8.bin -v -O -n 2 -t

echo "----------Test FMT YUV422 TILE 8X8 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -t

#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -t

#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV16@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_tile8x8.bin -v -O -n 2 -t

echo "----------Test FMT YUV444 TILE 8X8 8bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -t

#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -t

#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@NV24@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_tile8x8.bin -v -O -n 2 -t
#echo "test Tile8x8_YCbCr400: io -4 0xf9001000 0x0002115f;io -4 0xf9000000 0xffffffff"
#echo "io -4 0xf9001000 0x0002115f;io -4 0xf9000000 0xffffffff"
#sleep 3;

echo "----------Test FMT YUV420 TILE 8X8 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -t

#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -t

#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV15@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv420_10_tile8x8.bin -v -O -n 2 -t

echo "----------Test FMT YUV422 TILE 8X8 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -t

#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -t

#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV20@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv422_10_tile8x8.bin -v -O -n 2 -t

echo "----------Test FMT YUV444 TILE 8X8 10bit----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -t

#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:$HDISPLAY'x'$VDISPLAY+0+0@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -t

#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240:220x140+0+0@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:320x240@$NV30@tile8x8 -F  ../YUV_TILE/YUV_TILE_8X8/320x240_yuv444_10_tile8x8.bin -v -O -n 2 -t

#echo "test Tile8x8_YCbCr400_101010: io -4 0xf9001000 0x0002117f;io -4 0xf9000000 0xffffffff"
#echo "io -4 0xf9001000 0x0002117f;io -4 0xf9000000 0xffffffff"
#sleep 3;

echo "cluster $3  tile format test complete"
sleep 1;
