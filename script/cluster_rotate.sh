source ./set_config.sh
echo "Enter cluster rotate test case, Plane id: $1, crtc id: $2, connector id: $3"

if [ -n "$KERNEL_4_19_VERSION" ];then
	echo "it is 4.19"

	#argb888
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@AR24@afbc -F ../AFBCE/320x240_ARGB8888_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@AR24@afbc@rotate90 -F ../AFBCE/320x240_ARGB8888_afbc.bin -t 
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@AR24@afbc@rotate180 -F ../AFBCE/320x240_ARGB8888_afbc.bin -t 
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@AR24@afbc@rotate270 -F ../AFBCE/320x240_ARGB8888_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@AR24@afbc -F ../AFBCE/320x240_ARGB8888_afbc.bin -t

	#rgb888
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@RG24@afbc -F ../AFBCE/320x240_RGB888_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@RG24@afbc@rotate90 -F ../AFBCE/320x240_RGB888_afbc.bin -t 
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@RG24@afbc@rotate180 -F ../AFBCE/320x240_RGB888_afbc.bin -t 
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@RG24@afbc@rotate270 -F ../AFBCE/320x240_RGB888_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@RG24@afbc -F ../AFBCE/320x240_RGB888_afbc.bin -t

	#yuv420
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV12@afbc@rotate0 -F ../AFBCE/320x240_yuv420_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV12@afbc@rotate90 -F ../AFBCE/320x240_yuv420_afbc.bin -t 
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV12@afbc@rotate180 -F ../AFBCE/320x240_yuv420_afbc.bin -t 
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV12@afbc@rotate270 -F ../AFBCE/320x240_yuv420_afbc.bin -t 
	
	#yuv422
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV16@afbc -F ../AFBCE/320x240_yuv422_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV16@afbc@rotate90 -F ../AFBCE/320x240_yuv422_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV16@afbc@rotate180 -F ../AFBCE/320x240_yuv422_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV16@afbc@rotate270 -F ../AFBCE/320x240_yuv422_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NV16@afbc@rotate0 -F ../AFBCE/320x240_yuv422_afbc.bin -t
	
	#yuv420 10bit
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA12@afbc -F ../AFBCE/320x240_yuv420_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA12@afbc@rotate90 -F ../AFBCE/320x240_yuv420_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA12@afbc@rotate180 -F ../AFBCE/320x240_yuv420_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA12@afbc@rotate270 -F ../AFBCE/320x240_yuv420_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA12@afbc -F ../AFBCE/320x240_yuv420_10bit_afbc.bin -t	

	#yuv422 10bit
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA16@afbc -F ../AFBCE/320x240_yuv422_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA16@afbc@rotate90 -F ../AFBCE/320x240_yuv422_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA16@afbc@rotate180 -F ../AFBCE/320x240_yuv422_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA16@afbc@rotate270 -F ../AFBCE/320x240_yuv422_10bit_afbc.bin -t
	ovltest -M rockchip -s $CONNECTOR0@$CRTC0:$HDISPLAY'x'$VDISPLAY -P $CLUSTER0@$CRTC0:320x240:320'x'240@stride:320+0+0@NA16@afbc -F ../AFBCE/320x240_yuv422_10bit_afbc.bin -t

else
	echo "it isn't 4.19"
fi

echo "cluster $3 test complete"
sleep 1;
























