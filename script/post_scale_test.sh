source ./set_config.sh

if [ $# -eq 1 ];
then
    echo "$0 $1"
else
	echo "please assigned connector id: connector0: $0 0, or connector1: $0 1"
	exit
fi

echo "Enter POST SCALE test case"

if [ $1 = 0 ]; then
CONNECTOR_ID=$CONNECTOR0
CRTC_ID=$CRTC0
LAYER_ID=$CLUSTER0
echo "Test connector0:$CONNECTOR_ID"
elif [ $1 = 1 ]; then
CONNECTOR_ID=$CONNECTOR1
CRTC_ID=$CRTC1
LAYER_ID=$CLUSTER0
echo "Test connector1:$CONNECTOR_ID"
elif [ $1 = 2 ]; then
CONNECTOR_ID=$CONNECTOR2
CRTC_ID=$CRTC2
LAYER_ID=$ESMART0
echo "Test connector2:$CONNECTOR_ID"
else
echo "unkonw connector id"
fi

j=50
for i in `busybox seq 0 9`
do
	let j=j+5
	echo "set scale $j" 
	echo "ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $LAYER_ID@$CRTC_ID:800x1280+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin  -S $j:$j:$j:$j -t"
	ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $LAYER_ID@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -S $j:$j:$j:$j -t
done
