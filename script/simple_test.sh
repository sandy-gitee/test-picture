source ./set_config.sh $1

if [ $# -eq 1 ];
then
    echo "$0 $1"
else
	echo "please assigned connector id: connector0: $0 0, or connector1: $0 1"
	exit
fi

echo "Enter simple display test"

if [ $1 = 0 ]; then
CONNECTOR_ID=$CONNECTOR0
CRTC_ID=$CRTC0
echo "Test connector0:$CONNECTOR_ID"
elif [ $1 = 1 ]; then
CONNECTOR_ID=$CONNECTOR1
CRTC_ID=$CRTC1
echo "Test connector1:$CONNECTOR_ID"
else
echo "unkonw connector id"
fi

echo "ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART2@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin"
ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART2@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin
