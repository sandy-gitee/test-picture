#!/bin/sh
#SOC=`cat /sys/kernel/debug/dri/0/state  | grep Cluster3`
SOC=`modetest | grep SOC_ID -A 3 | grep value | awk -F '[ ]' '{print $2}' | awk 'NR==1'`
INTEL_SOC=`cat /proc/device-tree/compatible | grep socfpga`

echo $SOC
echo "$INTEL_SOC"

if [ ! -z "$INTEL_SOC" ];then
	echo "  xx $INTEL_SOC"
	BASE=$(printf "%u" 0xF9000000)
	VOP3=1
elif [ "$SOC" = "13666" ]; then
	SOC="rk3562"
	BASE=$(printf "%u" 0xF9000000)
	VOP3=1
elif [ "$SOC" = "13704" ]; then
	SOC="rk3588"
	BASE=$(printf "%u" 0xFDD90000)
	VOP3=0
else
	SOC="rk356x"
	BASE=$(printf "%u" 0xFE040000)
	VOP3=0
fi

echo $SOC $BASE
DSC=`expr $BASE + $(printf "%u" 0x200)`
OVL_SYS=`expr $BASE + $(printf "%u" 0x500)`
OVL_VP0=`expr $BASE + $(printf "%u" 0x600)`
OVL_VP1=`expr $BASE + $(printf "%u" 0x700)`
OVL=`expr $BASE + $(printf "%u" 0x600)`
VP0=`expr $BASE + $(printf "%u" 0xC00)`
VP1=`expr $BASE + $(printf "%u" 0xD00)`
VP2=`expr $BASE + $(printf "%u" 0xE00)`
VP3=`expr $BASE + $(printf "%u" 0xF00)`
C0=`expr $BASE + $(printf "%u" 0x1000)`
C1=`expr $BASE + $(printf "%u" 0x1200)`
C2=`expr $BASE + $(printf "%u" 0x1400)`
C3=`expr $BASE + $(printf "%u" 0x1600)`
E0=`expr $BASE + $(printf "%u" 0x1800)`
E1=`expr $BASE + $(printf "%u" 0x1A00)`
S0=`expr $BASE + $(printf "%u" 0x1C00)`
S1=`expr $BASE + $(printf "%u" 0x1E00)`
HDR=`expr $BASE + $(printf "%u" 0x2000)`
DSCPPS8K=`expr $BASE + $(printf "%u" 0x4000)`
DSCPPS4K=`expr $BASE + $(printf "%u" 0x4100)`

echo "SYS:"
io -r -4 -l 0x100 $BASE
echo ""
echo "DSC:"
io -r -4 -l 0x60 $DSC
echo ""

if [ "$VOP3" = "1" ];then
echo "OVL_SYS:"
io -r -4 -l 0x40 $OVL_SYS
echo ""

echo "OVL_VP0:"
io -r -4 -l 0x100 $OVL_VP0
echo ""

echo "OVL_VP1:"
io -r -4 -l 0x100 $OVL_VP1
echo ""
else
echo "OVL:"
io -r -4 -l 0x100 $OVL
echo ""
fi

echo "HDR:"
io -r -4 -l 0x40 $HDR
echo ""

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Video Port0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "VP0:"
	io -r -4 -l 0x100 $VP0
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Video Port1" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "VP1:"
	io -r -4 -l 0x100 $VP1
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Video Port2" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "VP2:"
	io -r -4 -l 0x100 $VP2
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Video Port3" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "VP3:"
	io -r -4 -l 0x100 $VP3
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Cluster0-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "Cluster0: "
	io -r -4 -l 0x130 $C0
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Cluster1-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "Cluster1: "
	io -r -4 -l 0x130 $C1
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Cluster2-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "Cluster2: "
	io -r -4 -l 0x130 $C2
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Cluster3-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "Cluster3: "
	io -r -4 -l 0x130 $C3
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Esmart0-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "Esmart0: "
	io -r -4 -l 0x100 $E0
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep "Esmart1-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	echo "Esmart1: "
	io -r -4 -l 0x100 $E1
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep -E "Smart0-win0|Esmart2-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	if [ "$SOC" = "rk356x" ]; then
		echo "Smart0: "
	else

		echo "Esmart2: "
	fi
	io -r -4 -l 0x100 $S0
	echo ""
fi

STAT=`cat /sys/kernel/debug/dri/0/summary  | grep -E "Smart1-win0|Esmart3-win0" | grep "ACTIVE"`
if [ "$1" = "a" ]; then
	STAT="ACTIVE"
fi
if  [ "$STAT" != "" ]; then
	if [ "$SOC" = "rk356x" ]; then
		echo "Smart1: "
	else

		echo "Esmart3: "
	fi

	io -r -4 -l 0x100 $S1
	echo ""
fi

if [ "$1" = "dsc8k" ]; then

	echo "DSCPPS8K"
	io -r -4 -l 0x100 $DSCPPS8K
	echo ""
fi

if [ "$1" = dsc4k]; then
	echo "DSCPPS4K:"
	io -r -4 -l 0x100 $DSCPPS4K
fi

cat /sys/kernel/debug/dri/0/summary
 