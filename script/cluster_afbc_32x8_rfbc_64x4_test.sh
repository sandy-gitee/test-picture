source ./set_config.sh
echo "Enter cluster afbc[32x8] and rfbc[64x4] test case, Plane id: $1, crtc id: $2, connector id: $3"

DELAY_TIME=2

echo "----------Test FMT ARGB888 afbc[32x8]----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@AB24@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA8888_afbc.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@AB24@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA8888_afbc.bin -t
sleep $DELAY_TIME;
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:800x1290+0+0@AB24@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA8888_afbc.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:800x1290+0+0@AB24@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA8888_afbc.bin -t
sleep $DELAY_TIME;
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:2560x1520:800x1280@stride:2560x1600+0+0@AB24@afbc32x8 -F  ../AFBC_32X8/dumplayer_1_2560x1600_RGBA8888_afbc_split_mode.bin.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:2560x1520:800x1280@stride:2560x1600+0+0@AB24@afbc32x8 -F ../AFBC_32X8/dumplayer_1_2560x1600_RGBA8888_afbc_split_mode.bin -t
sleep $DELAY_TIME;
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@AB24@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA8888_afbc.bin  -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@AB24@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA8888_afbc.bin  -v -O -n 2 -t

sleep $DELAY_TIME;
echo "----------Test FMT RGB888 afbc[32x8]----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1280+0+0@BG24@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGB888_afbc.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@BG24@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGB888_afbc.bin -t
sleep $DELAY_TIME;
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:800x1290+0+0@BG24@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGB888_afbc.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:800x1290+0+0@BG24@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGB888_afbc.bin -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:2560x1520:800x1280@stride:2560x1600+0+0@BG24@afbc32x8 -F  ../AFBC_32X8/dumplayer_1_2560x1600_RGB888_afbc_split_mode.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:2560x1520:800x1280@stride:2560x1600+0+0@BG24@afbc32x8 -F ../AFBC_32X8/dumplayer_1_2560x1600_RGB888_afbc_split_mode.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@BG24@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGB888_afbc.bin  -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@BG24@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGB888_afbc.bin  -v -O -n 2 -t

sleep $DELAY_TIME;

echo "----------Test FMT XRGB101010 afbc[32x8]----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1280+0+0@XB30@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA1010102_afbc.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@XR30@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA1010102_afbc.bin -t
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:800x1290+0+0@XB30@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA1010102_afbc.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:800x1290+0+0@XB30@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA1010102_afbc.bin -t
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:2560x1520:800x1280@stride:2560x1600+0+0@XB30@afbc32x8 -F  ../AFBC_32X8/dumplayer_1_2560x1600_RGBA1010102_afbc_split_mode.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:2560x1520:800x1280@stride:2560x1600+0+0@XB30@afbc32x8 -F ../AFBC_32X8/dumplayer_1_2560x1600_RGBA1010102_afbc_split_mode.bin -t

echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@XB30@afbc32x8 -F  ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA1010102_afbc.bin  -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:800x1290+0+0@XB30@afbc32x8 -F ../AFBC_32X8/800x1280_split_afbc_20230822/dumplayer_1_800x1280_RGBA1010102_afbc.bin  -v -O -n 2 -t

echo "----------Test FMT ARGB888 rfbc[64x4]----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin -t
sleep $DELAY_TIME;
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin -t
sleep $DELAY_TIME;
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:4096x2160:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:4096x2160:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin -t

sleep $DELAY_TIME;
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin  -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@AB24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888a8_4096x2160.bin  -v -O -n 2 -t
sleep $DELAY_TIME;

echo "----------Test FMT RGB888 rfbc[64x4]----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin -t
sleep $DELAY_TIME;
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin -t
sleep $DELAY_TIME;
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:4096x2160:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:4096x2160:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin -t

sleep $DELAY_TIME;
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin  -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@BG24@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgb888_4096x2160.bin  -v -O -n 2 -t
sleep $DELAY_TIME;

echo "----------Test FMT ARGB1010102 rfbc[64x4]----------:"
#no scale
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin -t
sleep $DELAY_TIME;
#scale up
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:200x320:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin -t
sleep $DELAY_TIME;
#scale down
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:4096x2160:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:4096x2160:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin -t

sleep $DELAY_TIME;
echo "ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin  -v -O -n 2 -t"
ovltest  -M rockchip -s $3@$2:$HDISPLAY'x'$VDISPLAY -P $1@$2:800x1280:800x1280@stride:4096x2160+0+0@XB30@rfbc -F  ../RFBC_64X4/rfbc_payload_128bit_aligned/Rk_afbc64x4_rgba1010102_4096x2160.bin  -v -O -n 2 -t
sleep $DELAY_TIME;

echo "cluster $3  tile format test complete"
sleep $DELAY_TIME;
