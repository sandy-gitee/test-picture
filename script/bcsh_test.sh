source ./set_config.sh


if [ $# -eq 1 ];
then
    echo "$0 $1"
else
	echo "please assigned connector id: connector0: $0 0, or connector1: $0 1"
	exit
fi

echo "Enter BCSH brightness test, before run this test, you should use ovltest to display simple UI on the screen"

if [ $1 = 0 ]; then
CONNECTOR_ID=$CONNECTOR0
echo "Test connector0:$CONNECTOR_ID"
elif [ $1 = 1 ]; then
CONNECTOR_ID=$CONNECTOR1
echo "Test connector1:$CONNECTOR_ID"
elif [ $1 = 2 ]; then
CONNECTOR_ID=$CONNECTOR2
echo "Test connector2:$CONNECTOR_ID"
else
echo "unkonw connector id"
fi

j=0
for i in `busybox seq 0 9`
do
	let j=j+10
	echo "->set connector: $CONNECTOR_ID brightness $j" 
	modetest -M rockchip -w $CONNECTOR_ID:brightness:$j
	sleep 1
done
echo "->set connector: $CONNECTOR_ID brightness 50" 
modetest -M rockchip -w $CONNECTOR_ID:brightness:50

echo "Enter BCSH contrast test"
j=0
for i in `busybox seq 0 9`
do
	let j=j+10
	echo "->set connector: $CONNECTOR_ID contrast $j" 
	modetest -M rockchip -w $CONNECTOR_ID:contrast:$j
	sleep 1
done
echo "->set connector: $CONNECTOR_ID contrast 50" 
modetest -M rockchip -w $CONNECTOR_ID:contrast:50

echo "Enter BCSH saturation test"
j=0
for i in `busybox seq 0 9`
do
	let j=j+10
	echo "->set connector: $CONNECTOR_ID saturation $j" 
	modetest -M rockchip -w $CONNECTOR_ID:saturation:$j
	sleep 1
done
echo "->set connector: $CONNECTOR_ID saturation 50" 
modetest -M rockchip -w $CONNECTOR_ID:saturation:50

echo "Enter BCSH hue test"
j=0
for i in `busybox seq 0 9`
do
	let j=j+10
	echo "->set connector: $CONNECTOR_ID hue $j" 
	modetest -M rockchip -w $CONNECTOR_ID:hue:$j
	sleep 1
done
echo "->set connector: $CONNECTOR_ID hue 50" 
modetest -M rockchip -w $CONNECTOR_ID:hue:50
