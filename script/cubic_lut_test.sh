source ./set_config.sh

if [ $# -eq 1 ];
then
    echo "$0 $1"
else
	echo "please assigned connector id: connector0: $0 0, or connector1: $0 1"
	exit
fi

echo "Enter Cubic Lut Test"

if [ $1 = 0 ]; then
CONNECTOR_ID=$CONNECTOR0
CRTC_ID=$CRTC0
echo "Test connector0:$CONNECTOR_ID"
elif [ $1 = 1 ]; then
CONNECTOR_ID=$CONNECTOR1
CRTC_ID=$CRTC1
echo "Test connector1:$CONNECTOR_ID"
else
echo "unkonw connector id"
fi

echo "ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 1"
ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 1 -t

echo "ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 2"
ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 2 -t

echo "ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 3"
ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 3 -t

echo "ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 4"
ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 4 -t

echo "ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 1"
ovltest  -M rockchip -s $CONNECTOR_ID@$CRTC_ID:$HDISPLAY'x'$VDISPLAY -P $ESMART0@$CRTC_ID:800x1280:$HDISPLAY'x'$VDISPLAY+0+0@AB24 -F ../ARGB/800x1280-AB24-Alauncher-blue.bin -U 1 -t
