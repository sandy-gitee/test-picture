source ./set_config.sh

#echo "-----------Enter VP0 plane test-------------"
#echo "./esmart_test.sh $ESMART0 $CRTC0 $CONNECTOR0"
#./esmart_test.sh $ESMART0 $CRTC0 $CONNECTOR0
#echo "./esmart_test.sh $ESMART1 $CRTC0 $CONNECTOR0"
#./esmart_test.sh $ESMART1 $CRTC0 $CONNECTOR0
#echo "./esmart_test.sh $ESMART2 $CRTC0 $CONNECTOR0"
#./esmart_test.sh $ESMART2 $CRTC0 $CONNECTOR0
#echo "./esmart_test.sh $ESMART3 $CRTC0 $CONNECTOR0"
#./esmart_test.sh $ESMART3 $CRTC0 $CONNECTOR0
 
echo "-----------Enter VP1 plane test-------------"
echo "./esmart_test.sh $ESMART0 $CRTC1 $CONNECTOR1"
./esmart_test.sh $ESMART0 $CRTC1 $CONNECTOR1
echo "./esmart_test.sh $ESMART1 $CRTC1 $CONNECTOR1"
./esmart_test.sh $ESMART1 $CRTC1 $CONNECTOR1
echo "./esmart_test.sh $ESMART2 $CRTC0 $CONNECTOR1"
./esmart_test.sh $ESMART2 $CRTC1 $CONNECTOR1
echo "./esmart_test.sh $ESMART3 $CRTC1 $CONNECTOR1"
./esmart_test.sh $ESMART3 $CRTC1 $CONNECTOR1

echo "-----------Enter VP0 overlay test-------------"
echo "./overlay_test.sh  0"
./overlay_test.sh  0

echo "-----------Enter VP1 overlay test-------------"
echo "./overlay_test.sh  1"
./overlay_test.sh  1

echo "-----------Enter VP0 gamma test-------------"
echo "./gamma_test.sh  0"
./gamma_test.sh  0

echo "-----------Enter VP1 gamma test-------------"
echo "./gamma_test.sh  1"
./gamma_test.sh  1

echo "-----------Enter VP0 post scale test-------------"
echo "./post_scale_test.sh  0"
./post_scale_test.sh  0

echo "-----------Enter VP1 post scale test-------------"
echo "./post_scale_test.sh  1"
./post_scale_test.sh  1

echo "-----------Enter VP0 cubic lut test-------------"
echo "./cubic_lut_test.sh  0"
./cubic_lut_test.sh  0

echo "-----------Enter VP1 cubic lut test---------"
./cubic_lut_test.sh 1

echo "-----------Enter writeback test-------------"
echo "./wb_test.sh"
./wb_test.sh

echo "-----------Enter plane move test-------------"
echo "./plane_move_test.sh"
./plane_move_test.sh
