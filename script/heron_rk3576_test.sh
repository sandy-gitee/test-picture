source ./set_config.sh

echo "-------------------vp0 test-------------------"
./esmart_test.sh $ESMART0 $CRTC0 $CONNECTOR0  //./esmart_test.sh 57 71 172
./esmart_test.sh $ESMART2 $CRTC0 $CONNECTOR0  //./esmart_test.sh 102 71 172
./cluster_test.sh $CLUSTER0 $CRTC0 $CONNECTOR0  //./cluster_test.sh 137 71 172
./cluster_test.sh $CLUSTER1 $CRTC0 $CONNECTOR0  //./cluster_test.sh 151 71 172
./cluster_afbc_32x8_rfbc_64x4_test.sh $CLUSTER0 $CRTC0 $CONNECTOR0  //./cluster_afbc_32x8_rfbc_64x4_test.sh 137 71 172
./cluster_afbc_32x8_rfbc_64x4_test.sh $CLUSTER1 $CRTC0 $CONNECTOR0  //./cluster_afbc_32x8_rfbc_64x4_test.sh 151 71 172
./cluster_tile_test.sh $CLUSTER0 $CRTC0 $CONNECTOR0  //./cluster_tile_test.sh 137 71 172
./cluster_tile_test.sh $CLUSTER1 $CRTC0 $CONNECTOR0  //./cluster_tile_test.sh 151 71 172
./overlay_test_for_heron_rk3576.sh 0
./post_scale_test.sh 0
./gamma_test.sh 0
./wb_test.sh
 
io -4 0xf9000024 0x10;//selete axi0
./cubic_lut_test.sh 0
io -4 0xf9000024 0x210
 
echo "-------------------vp1 test-------------------"
./esmart_test.sh $ESMART1 $CRTC1 $CONNECTOR1  //./esmart_test.sh 81 95 174
./esmart_test.sh $ESMART3 $CRTC1 $CONNECTOR1  //./esmart_test.sh 123 95 174
./cluster_test.sh $CLUSTER0 $CRTC1 $CONNECTOR1  //./cluster_test.sh 137 95 174
./cluster_test.sh $CLUSTER1 $CRTC1 $CONNECTOR1  //./cluster_test.sh 151 95 174
./cluster_afbc_32x8_rfbc_64x4_test.sh $CLUSTER0 $CRTC1 $CONNECTOR1  //./cluster_afbc_32x8_rfbc_64x4_test.sh 137 95 174
./cluster_afbc_32x8_rfbc_64x4_test.sh $CLUSTER1 $CRTC1 $CONNECTOR1  //./cluster_afbc_32x8_rfbc_64x4_test.sh 151 95 174
./cluster_tile_test.sh $CLUSTER0 $CRTC1 $CONNECTOR1  //./cluster_tile_test.sh 137 95 174
./cluster_tile_test.sh $CLUSTER1 $CRTC1 $CONNECTOR1  //./cluster_tile_test.sh 151 95 174
./overlay_test_for_heron_rk3576.sh 1
./post_scale_test.sh 1
./gamma_test.sh 1
#vp1.sh&;bcsh_test.sh 1&
 
echo "-------------------vp2 test-------------------"
./esmart_test.sh $ESMART0 $CRTC2 $CONNECTOR2  //./esmart_test.sh 57 116 176
./esmart_test.sh $ESMART1 $CRTC2 $CONNECTOR2  //./esmart_test.sh 81 116 176
./esmart_test.sh $ESMART2 $CRTC2 $CONNECTOR2  //./esmart_test.sh 102 116 176
./esmart_test.sh $ESMART3 $CRTC2 $CONNECTOR2  //./esmart_test.sh 123 116 176
./overlay_test_for_heron_rk3576.sh 2
./post_scale_test.sh 2
./gamma_test.sh 2
#vp2.sh&;bcsh_test.sh 2&